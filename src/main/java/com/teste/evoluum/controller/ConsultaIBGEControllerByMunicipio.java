package com.teste.evoluum.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teste.evoluum.domain.MunicipioDTO;
import com.teste.evoluum.service.IBGEService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/ibge")
@Api(value = "Consulta IBGE")
public class ConsultaIBGEControllerByMunicipio {
	
	private Logger logger = LoggerFactory.getLogger(ConsultaIBGEControllerByMunicipio.class);
	
	@Autowired
	private IBGEService ibgeService;
	
	
	
	@GetMapping("/municipio/{nomeCidade}")
	@ApiOperation(value = "Busca de município por nome. As respostas são armazenadas em cache que é renovado a cada minuto.")
	public ResponseEntity<List<MunicipioDTO>> getIdMunicipio(@PathVariable String nomeCidade){
		logger.info("Iniciando requisição para nome de cidade.");
		List<MunicipioDTO> listaMunicipioDTO = ibgeService.getIdMunicipio(nomeCidade.trim().toLowerCase());
		return ResponseEntity.status(HttpStatus.OK).body(listaMunicipioDTO);
	}
}