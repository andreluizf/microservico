package com.teste.evoluum.controller;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teste.evoluum.service.IBGEService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/ibge")
@Api(value = "Consulta IBGE")
public class ConsultaIBGEControllerJson {
	
	private Logger logger = LoggerFactory.getLogger(ConsultaIBGEControllerJson.class);
	
	@Autowired
	private IBGEService ibgeService;
	
	@GetMapping(value = "/all/json")
	@ApiOperation(value = "Retorna todos os municípios. Em caso de timeout de 15seg uma lista vazia é retornada.")
	public ResponseEntity<Object> getFindAll(HttpServletResponse response){
		logger.info("Iniciando requisição para todos os dados e retornando em JSON");
		ibgeService.find("json" , response);
		return ResponseEntity.status(response.getContentType() == null? HttpStatus.NOT_FOUND : HttpStatus.OK).body(null);
	}
	
}