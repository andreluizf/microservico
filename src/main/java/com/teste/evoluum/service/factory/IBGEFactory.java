package com.teste.evoluum.service.factory;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.teste.evoluum.service.interfaces.IBGEAdjust;

@Component
public class IBGEFactory {
	
	public Optional< IBGEAdjust> getRetorno(String tipo) {		
        if (tipo.equals("csv"))
            return Optional.ofNullable(new IBGERetornoCSV());  
        if (tipo.equals("json"))
            return Optional.ofNullable(new IBGERetornoJSON()); 
        return Optional.ofNullable(null);
    }
}