package com.teste.evoluum.service.factory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.teste.evoluum.domain.LocalDTO;
import com.teste.evoluum.service.interfaces.IBGEAdjust;

public class IBGERetornoCSV implements  IBGEAdjust {

	private Logger logger = LoggerFactory.getLogger(IBGERetornoCSV.class);
	private static final String FILE_NAME = "localidades.csv";
	
	@Override
	public void adjustData(List<LocalDTO> list, HttpServletResponse response) {
		try {
			logger.info("Gerando arquivo CSV.");
			gerarAquivo(response, gerarTexto(list));
			logger.info("Arquivo CSV gerado com sucesso.");
		} catch (IOException e) {
			logger.info("Falha na geração do arquivo CSV.");
			logger.error(e.getMessage());
		}
	}

	public void gerarAquivo(HttpServletResponse response, StringBuilder texto) throws IOException {
		byte[] csv = texto.toString().getBytes(StandardCharsets.UTF_8);
		response.setContentType("text/csv");
		response.setHeader("Content-Disposition", "attachment; filename=" + FILE_NAME);
		response.setContentLength(csv.length);
		response.getOutputStream().write(csv);
	}

	public StringBuilder gerarTexto(List<LocalDTO> list) {
		StringBuilder texto = new StringBuilder();
		texto.append("idEstado,siglaEstado,regiaoNome,nomeCidade,nomeMesorregiao,nomeFormatado\n");
		list.forEach(localDTO -> texto.append(localDTO.getIdEstado() + "," + localDTO.getSiglaEstado() + ","
				+ localDTO.getRegiaoNome() + "," + localDTO.getNomeCidade() + "," + localDTO.getNomeMesorregiao() + ","
				+ localDTO.getNomeFormatado() + "\n"));
		return texto;
	}
}