package com.teste.evoluum.service.factory;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teste.evoluum.domain.LocalDTO;
import com.teste.evoluum.service.interfaces.IBGEAdjust;

public class IBGERetornoJSON implements IBGEAdjust {

	private Logger logger = LoggerFactory.getLogger(IBGERetornoJSON.class);
	
	@Override
	public void adjustData(List<LocalDTO> list, HttpServletResponse response) {
		try {
			logger.info("Gerando JSON.");
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(new ObjectMapper().writeValueAsString(list));
			logger.info("JSON gerado com sucesso.");
		} catch (IOException e) {
			logger.info("Falha na geração do JSON.");
			logger.error(e.getMessage());
		}	
	}
}
