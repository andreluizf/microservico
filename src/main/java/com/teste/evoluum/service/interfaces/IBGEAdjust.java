package com.teste.evoluum.service.interfaces;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.teste.evoluum.domain.LocalDTO;

public interface IBGEAdjust {
	public void adjustData(List<LocalDTO> list, HttpServletResponse response);
}