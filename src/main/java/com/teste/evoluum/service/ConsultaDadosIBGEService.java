package com.teste.evoluum.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.teste.evoluum.domain.Estado;
import com.teste.evoluum.domain.LocalDTO;
import com.teste.evoluum.domain.Municipio;
import com.teste.evoluum.domain.MunicipioDTO;
import com.teste.evoluum.utils.NormalizeUtils;

@Service
public class ConsultaDadosIBGEService {

	private Logger logger = LoggerFactory.getLogger(ConsultaDadosIBGEService.class);

	@Value("${endpoint.ibge.estados:https://servicodados.ibge.gov.br/api/v1/localidades/estados}")
	private String estadosEndpoint;

	@Value("${endpoint.ibge.municipios:https://servicodados.ibge.gov.br/api/v1/localidades/estados/%d/municipios}")
	private String municipiosEndpoint;

	@Value("${endpoint.ibge.todosMunicipios:https://servicodados.ibge.gov.br/api/v1/localidades/municipios}")
	private String todasLocalidadesEndpoint;

	@Autowired
	private RestTemplate restTemplate;

	@HystrixCommand(fallbackMethod = "getLocalFindAllEmpty", commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "15000") })
	public List<LocalDTO> getLocalFindAll() {
		List<LocalDTO> listaLocais = new ArrayList<>();

		logger.info("Lendo estados.");
		List<Estado> listaEstados = Arrays.asList(restTemplate.getForEntity(estadosEndpoint, Estado[].class).getBody());
		logger.info("Estados lidos com sucesso.");

		logger.info("Lendo municipios de cada estado.");
		listaEstados.forEach(estado -> {
			List<Municipio> listaMunicipios = Arrays.asList(restTemplate
					.getForEntity(String.format(municipiosEndpoint, estado.getId()), Municipio[].class).getBody());
			listaMunicipios.forEach(municipioDTO -> listaLocais.add(new LocalDTO().estado(estado).municipio(municipioDTO)));
		});
		logger.info("Municipios lidos com sucesso.");

		return listaLocais;
	}

	public List<LocalDTO> getLocalFindAllEmpty() {
		return new ArrayList<>();
	}
	
	public List<MunicipioDTO> getMunicipioEmpty(String msg) {
		return new ArrayList<>();
	}

	@Cacheable("idMunicipio")
	@HystrixCommand(fallbackMethod = "getMunicipioEmpty", commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "15000") })
	public List<MunicipioDTO> getIdMunicipio(String nomeCidade) {
		logger.info("Lendo municipios.");
		List<Municipio> listaMunicipios = Arrays
				.asList(restTemplate.getForEntity(todasLocalidadesEndpoint, Municipio[].class).getBody());
		logger.info("Municipios lidos com sucesso.");
		return listaMunicipios.stream().filter(municipio -> NormalizeUtils.removerAcentos(municipio.getNome()).equalsIgnoreCase(nomeCidade))
				.map(municipio -> new MunicipioDTO().id(municipio.getId())).collect(Collectors.toList());
	}
	
	
}
