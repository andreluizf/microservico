package com.teste.evoluum.service;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teste.evoluum.domain.MunicipioDTO;
import com.teste.evoluum.service.factory.IBGEFactory;
import com.teste.evoluum.service.interfaces.IBGEAdjust;

@Service
public class IBGEService {

	private Logger logger = LoggerFactory.getLogger(IBGEService.class);

	@Autowired
	private IBGEFactory retornoFactory;

	@Autowired
	private ConsultaDadosIBGEService consultaIbgeService;

	public void find(String tipo, HttpServletResponse response) {
		Optional<IBGEAdjust> optionalRetorno = retornoFactory.getRetorno(tipo);
		if (optionalRetorno.isPresent()) {
			if (logger.isInfoEnabled() && tipo != null)
				logger.info(String.format("Consultando locais IBGE:  %s ", tipo));
			optionalRetorno.get().adjustData(consultaIbgeService.getLocalFindAll(), response);
		}
	}

	public List<MunicipioDTO> getIdMunicipio(String nomeCidade) {
		return consultaIbgeService.getIdMunicipio(nomeCidade);
	}
}