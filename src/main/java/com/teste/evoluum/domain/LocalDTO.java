package com.teste.evoluum.domain;

public class LocalDTO {
	private int idEstado;
	private String siglaEstado;
	private String regiaoNome;
	private String nomeCidade;
	private String nomeMesorregiao;
	
	
	public LocalDTO estado(Estado estado) {
		this.idEstado = estado.getId();
		this.siglaEstado = estado.getSigla();
		this.regiaoNome = estado.getRegiao().getNome();
		return this;
	}

	public LocalDTO municipio(Municipio municipio) {
		this.nomeCidade = municipio.getNome();
		this.nomeMesorregiao = municipio.getMicrorregiao().getMesorregiao().getNome();
		return this;
	}
	

	public String getNomeFormatado() {
		return nomeCidade + "/" + siglaEstado;
	}

	public int getIdEstado() {
		return idEstado;
	}

	public String getSiglaEstado() {
		return siglaEstado;
	}

	public String getRegiaoNome() {
		return regiaoNome;
	}

	public String getNomeCidade() {
		return nomeCidade;
	}

	public String getNomeMesorregiao() {
		return nomeMesorregiao;
	}
}