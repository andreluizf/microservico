package com.teste.evoluum.domain;

import java.io.Serializable;

public class Estado implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String sigla;
	private String nome;
	private Regiao regiao;



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Regiao getRegiao() {
		return regiao;
	}

	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}

	@Override
	public String toString() {
		return "Estado{" + "id=" + id + ", sigla='" + sigla + '\'' + ", nome='" + nome + '\'' + ", regiao=" + regiao
				+ '}';
	}
}
