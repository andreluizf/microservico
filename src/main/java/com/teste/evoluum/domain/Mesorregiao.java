package com.teste.evoluum.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Mesorregiao implements Serializable {
	
	private static final long serialVersionUID = 1L;
    private int id;
    private String nome;
    @JsonProperty(value = "UF")
    private Estado estado;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estado getUF() {
        return estado;
    }

    public void setUF(Estado estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Mesorregiao{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", UF=" + estado +
                '}';
    }
}
