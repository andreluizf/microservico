package com.teste.evoluum.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author André
 *
 */
@Configuration
@EnableCaching
public class CacheConfig {

	
}