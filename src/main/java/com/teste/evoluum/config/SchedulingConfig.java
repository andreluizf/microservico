package com.teste.evoluum.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 
 * @author André
 *
 */
@Configuration
@EnableScheduling
public class SchedulingConfig {
	@Autowired
	private CacheManager cacheManager;

	@Scheduled(fixedRate = 60000)
	public void cleanCache() {
		cacheManager.getCacheNames().forEach(nome -> cacheManager.getCache(nome).clear());
	}

}