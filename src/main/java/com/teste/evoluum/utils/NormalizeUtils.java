package com.teste.evoluum.utils;

import java.text.Normalizer;

public class NormalizeUtils {
	public static String removerAcentos(String str) {
		return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}
}
