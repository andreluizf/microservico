package com.teste.evoluum.controller;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import com.teste.evoluum.AbstractSpringIntegration;

public class ConsultaIBGEControllerByMunicipioTest extends AbstractSpringIntegration{

	
	@Test
	public void testAddEmployee() {
		ResponseEntity<Object> responseEntity = restTemplate
				.getForEntity("http://localhost:" + port + "/api/ibge/municipio/sao paulo", null, Object.class);
		assertEquals(200, responseEntity.getStatusCodeValue());
	}
}