package com.teste.evoluum.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import com.teste.evoluum.AbstractSpringIntegration;
import com.teste.evoluum.EvoluumApplication;

public class ConsultaIBGEControllerJsonTest  extends AbstractSpringIntegration {

	@Test
	public void testAddEmployee() {
		ResponseEntity<Object> responseEntity = this.restTemplate
				.getForEntity("http://localhost:" + port + "/api/ibge/all/csv", null, Object.class);
		assertEquals(200, responseEntity.getStatusCodeValue());
	}
}