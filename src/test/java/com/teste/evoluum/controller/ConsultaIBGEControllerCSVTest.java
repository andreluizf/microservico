package com.teste.evoluum.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import com.teste.evoluum.AbstractSpringIntegration;

public class ConsultaIBGEControllerCSVTest  extends AbstractSpringIntegration {
	

	@Test
	public void testAddEmployee() {
		ResponseEntity<Object> responseEntity = this.restTemplate
				.getForEntity("http://localhost:" + port + "/api/ibge/all/json", null, Object.class);
		assertEquals(200, responseEntity.getStatusCodeValue());
	}
}