package com.teste.evoluum.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.teste.evoluum.domain.Estado;
import com.teste.evoluum.domain.Mesorregiao;
import com.teste.evoluum.domain.Microrregiao;
import com.teste.evoluum.domain.Municipio;
import com.teste.evoluum.domain.Regiao;



@SpringBootTest
public class ConsultaDadosIBGEServiceTest {
	
	private static final int ESTADO_ID = 1;
	private static final String ESTADO_NOME = "nome estado";
	
	@SpyBean
	private ConsultaDadosIBGEService consultaDadosIBGEService;
	
	@MockBean
    private RestTemplate restTemplate;
	
	@Value("${endpoint.ibge.estados:https://servicodados.ibge.gov.br/api/v1/localidades/estados}")
	private String estadosEndpoint;

	@Value("${endpoint.ibge.municipios:https://servicodados.ibge.gov.br/api/v1/localidades/estados/%d/municipios}")
	private String municipiosEndpoint;

	@Value("${endpoint.ibge.todosMunicipios:https://servicodados.ibge.gov.br/api/v1/localidades/municipios}")
	private String todasLocalidadesEndpoint;

	
	@Test
	public void testGetTodosOsDados() {
		Estado[] arrayEstado = new Estado[1];
		arrayEstado[0] = gerarEstado();
		ResponseEntity<Estado[]> responseEntityEstado = new ResponseEntity<Estado[]>(arrayEstado, HttpStatus.OK);
		when(restTemplate.getForEntity(estadosEndpoint, Estado[].class)).thenReturn(responseEntityEstado);
		
		Municipio[] arrayMunicipio = new Municipio[1];
		arrayMunicipio [0] = gerarMunicipio();
		ResponseEntity<Municipio[]> responseEntityMunicipio = new ResponseEntity<Municipio[]>(arrayMunicipio, HttpStatus.OK);
		when(restTemplate.getForEntity(String.format(municipiosEndpoint, ESTADO_ID), Municipio[].class)).thenReturn(responseEntityMunicipio);
			
		assertNotNull(consultaDadosIBGEService.getLocalFindAll());
	}

	private Municipio gerarMunicipio() {
		Municipio municipio = new Municipio();
		Microrregiao microrregiao = new Microrregiao();
		Mesorregiao mesorregiao = new Mesorregiao();
		
		mesorregiao.setNome("mesorregiao teste");
		microrregiao.setMesorregiao(mesorregiao);
		
		municipio.setMicrorregiao(microrregiao);
		municipio.setNome(ESTADO_NOME);
		
		return municipio;
	}

	private Estado gerarEstado() {
		Estado estado = new Estado();
		estado.setId(ESTADO_ID);
		Regiao regiao = new Regiao();
		
		regiao.setNome("regiao teste");
		estado.setRegiao(regiao);
		
		return estado;
	}
	
	@Test
	public void testGetIdMunicipio() {
		Municipio[] arrayMunicipio = new Municipio[1];
		arrayMunicipio[0] = gerarMunicipio();
		ResponseEntity<Municipio[]> responseEntityMunicipio= new ResponseEntity<Municipio[]>(arrayMunicipio, HttpStatus.OK);
		when(restTemplate.getForEntity(todasLocalidadesEndpoint, Municipio[].class)).thenReturn(responseEntityMunicipio);	
		assertNotNull(consultaDadosIBGEService.getIdMunicipio(ESTADO_NOME));
	}
}