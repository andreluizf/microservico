package com.teste.evoluum.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

import com.teste.evoluum.domain.MunicipioDTO;
import com.teste.evoluum.service.factory.IBGEFactory;
import com.teste.evoluum.service.factory.IBGERetornoCSV;

@SpringBootTest
public class IbgeServiceTest {
	
	@SpyBean
	private IBGEService ibgeService;
	
	@MockBean
	private IBGEFactory ibgeFactory;
	
	@MockBean
	private HttpServletResponse response;
	
	@MockBean
	private ConsultaDadosIBGEService consultaDadosIBGEService;
	
	@Test
	public void testGetTodosOsDados() {
		IBGERetornoCSV retorno = Mockito.mock(IBGERetornoCSV.class);
		doReturn(Optional.of(retorno)).when(ibgeFactory).getRetorno("json");
		doNothing().when(Optional.of(retorno).get()).adjustData(any(), any());
		ibgeService.find("json", response);
	}
	
	@Test
	public void testGetIdMunicipio() {
		doReturn(new ArrayList<MunicipioDTO>()).when(consultaDadosIBGEService).getIdMunicipio(any());
		assertNotNull(ibgeService.getIdMunicipio("nome"));
	}
}
