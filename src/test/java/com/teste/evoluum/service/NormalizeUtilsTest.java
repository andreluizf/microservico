package com.teste.evoluum.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.teste.evoluum.utils.NormalizeUtils;

public class NormalizeUtilsTest {
	@Test
	public void normalizarCidade() {
		assertEquals(NormalizeUtils.removerAcentos("São Paulo"),"Sao Paulo" );
		
		assertEquals(NormalizeUtils.removerAcentos("Florianópolis"),"Florianopolis" );
		
		assertEquals(NormalizeUtils.removerAcentos("Maringá"),"Maringa" );
	}

	
}
