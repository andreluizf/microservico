package com.teste.evoluum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(classes = EvoluumApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class AbstractSpringIntegration  {

	@LocalServerPort
	protected int port;

	@Autowired
	protected TestRestTemplate restTemplate;
}